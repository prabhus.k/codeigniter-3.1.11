<div class="footer-new">
<footer class="footer-half">
<section class="contact-info">
<div class="container">
<div class="col-md-5 col-sm-9 col-xs-12">
<img src="https://tax2win.in/assets-new/img/calling.png" alt="File ITR Support" class="img-responsive" title="File ITR Support" width="40"> <span>
<a href="tel:+91 9660-99-66-55" style="color: #000;">+91 9660-99-66-55</a><small>&nbsp;|&nbsp;</small><a href="mailto:support@tax2win.in" data-ctid="email-link-support-tax2win-in-37ad9c1d10">support@tax2win.in</a> </span>
</div>
<div class="col-md-3 col-sm-3 col-xs-12">
<div class="social-icons">
<a href="https://www.facebook.com/tax2win" onclick="if (!window.__cfRLUnblockHandlers) return false; window.open('https://www.facebook.com/tax2win', '_blank', 'width=800,height=600,scrollbars=yes,menubar=no,status=yes,resizable=yes,screenx=0,screeny=0'); return false;" rel="nofollow" title="Facebook" style="padding-right: 10px;"><img src="/codeigniter-3.1.11/fb.png" style="height: 22px;"></a><a href="https://twitter.com/tax2win" onclick="if (!window.__cfRLUnblockHandlers) return false; window.open('https://twitter.com/tax2win', '_blank', 'width=800,height=600,scrollbars=yes,menubar=no,status=yes,resizable=yes,screenx=0,screeny=0'); return false;" rel="nofollow" title="Twitter" style="padding-right: 10px;"><img src="/codeigniter-3.1.11/twitter.png"></i></a><a href="https://www.linkedin.com/company/tax2win" onclick="if (!window.__cfRLUnblockHandlers) return false; window.open('https://www.linkedin.com/company/tax2win', '_blank', 'width=800,height=600,scrollbars=yes,menubar=no,status=yes,resizable=yes,screenx=0,screeny=0'); return false;" rel="nofollow" title="Linkedin" style="padding-right: 10px;"><img src="/codeigniter-3.1.11/ln.png"></a><a href="https://www.youtube.com/channel/UCI5JZW94yuU6pxH6yKTwuOw/featured" onclick="if (!window.__cfRLUnblockHandlers) return false; window.open('https://www.youtube.com/channel/UCI5JZW94yuU6pxH6yKTwuOw/featured', '_blank', 'width=800,height=600,scrollbars=yes,menubar=no,status=yes,resizable=yes,screenx=0,screeny=0'); return false;" rel="nofollow" title="Google Plus" style="padding-right: 10px;"><img src="/codeigniter-3.1.11/youtube.jpg"></a><a href="https://www.instagram.com/tax2win/" onclick="if (!window.__cfRLUnblockHandlers) return false; window.open('https://www.instagram.com/tax2win/', '_blank', 'width=800,height=600,scrollbars=yes,menubar=no,status=yes,resizable=yes,screenx=0,screeny=0'); return false;" rel="nofollow" title="Instagram" style="padding-right: 10px;"><img src="/codeigniter-3.1.11/insta.png"></a> </div>
</div>
<div class="col-md-4 col-sm-3 col-xs-12">
<figure class="certificate">
<img src="https://tax2win.in/assets-new/img/certificate.jpg" alt="">
</figure>
</div>
</div>
</section>
<section class="copyright">
<div class="container">
<div class="row">
<div class="col-md-6 col-sm-6 col-xs-12">
© 2021 <a href="https://tax2win.in/" title="File Income Tax Return">TAX2WIN</a>. All Rights Reserved.
</div>
<div class="col-md-6 col-sm-6 col-xs-12 text-right">
<a href="https://tax2win.in/legal/terms-n-conditions">Terms &amp; Conditions</a> | <a href="https://tax2win.in/legal/privacy-policy">Privacy Policy</a> </div>
</div>
</div>
</section>
<div class="baloon-left balloon"></div>
<div class="baloon-right balloon"></div>
</footer>
</div>
</body>
<script src="<?=base_url()?>assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/readydoc.js'); ?>" type="text/javascript"></script>
</html>
