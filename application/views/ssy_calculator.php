<section class="ssy_calculator">
  <div class="container-fluid">
    <div class="row">
<div class="ssy_cal-wrapper">
<div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 left">
<h1>Sukanya Samriddhi Yojana Calculator</h1>
<span>Helps you calculate the total amount invested, returns, maturity value and maturity date.</span>
<h4>Sukanya Samriddhi Yojna is a post office savings scheme of the government to encourage Indians to save money for the future of their girl children. This scheme is backed by the sovereign guarantee.</h4>
<ul>
<li>Investment : Minimum Rs.250 to maximum Rs,150,000 should be deposited every year for 14 years from the date of opening.</li>
<li>Interest Rate : Declared by the government every quarter. Current interest rate is 8.40% per annum compounded annually.</li>
<li>Tax Benefit : Investment provides tax deduction u/s 80 C upto Rs.1,50,0000. Interest and Maturity amount is also tax exempt.Thus, enjoys exempt-exempt-exempt (EEE) tax status.</li>
</ul>
</div>
<div class="col-lg-7 col-md-6 col-sm-12 col-xs-12 right">
<div class="row">
<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">

<?php echo form_open('SSY_Calculator');?>

<div class="form-group col-md-12">
<label for="email">Email <i><img src="<?php echo site_url('mail.svg'); ?>" alt=""></i></label>
<input type="text" name="ssy_email" id="ssy_email" class="email form-control input_change" placeholder="Email" autofocus="" spellcheck="false" value="<?php echo set_value('ssy_email'); ?>">
<?php echo form_error('ssy_email', '<span style="color:red !important;font-size:11px !important;" id="email_error" class="error" for="ssy_email">','</span>'); ?>

</div>
<div class="form-group col-md-6">
<label for="child-age">Date of Birth <i><img src="<?php echo site_url('green-calendar.svg'); ?>" alt=""></i></label>
<input type="text" name="ssy_dob" class="child_age form-control input_change" placeholder="date of birth child" id="ssy_dob" readonly="" style="background:white;" value="<?php echo set_value('ssy_dob'); ?>">

<?php echo form_error('ssy_dob', '<span style="color:red;font-size:11px;" id="dob_error" class="error" for="dob_error">','</span>'); ?>
</div>
<div class="form-group col-md-6">
<label for="child-age">Date of deposit <i><img src="<?php echo site_url('green-calendar.svg'); ?>" alt=""></i></label>
<input type="text" name="ssy_dodeposit" class="dod form-control input_change datepicker-orient-bottom" id="ssy_dodeposit" placeholder="date of diposit" readonly="" style="background:white;"  value="<?php echo set_value('ssy_dodeposit'); ?>">

<?php echo form_error('ssy_dodeposit', '<span style="color:red;font-size:11px;" id="dod_error" class="error" for="ssy_dodeposit">','</span>'); ?>

</div>
<div class="form-group col-md-12">
<label for="child-age">Deposit Yearly Amount <i><img src="<?php echo site_url('rupee-icon.svg'); ?>" alt=""></i></label>
<input type="number" name="ssy_deposit_amount_yearly" class="yearly_amount form-control input_change" placeholder="Deposit Amount" id="ssy_deposit_amount_yearly" style="padding: 5px 0;" value="<?php echo set_value('ssy_deposit_amount_yearly'); ?>">


<?php echo form_error('ssy_deposit_amount_yearly', '<span style="color:red;font-size:11px;" id="amount_error" class="error" for="ssy_deposit_amount_yearly">','</span>'); ?>

<small id="amount_def">A minimum of Rs.250 and a maximum of Rs.1,50,000 should be deposited every year</small>
</div>
<br class="hidden-xs">
<div class="form-group col-md-5">
<button type="submit" id="ssy_calculate" class="green-btn">Calculate</button>
</div>
<div class="col-lg-7">

</div>
<?php echo form_close(); ?>


</div>
<div class="col-lg-4 hidden-md hidden-sm hidden-xs"><span class="prcent_sign">%</span></div>
<img src="<?php echo site_url('girls-symbol.png'); ?>" alt="" class="girl_symbol"> </div>
</div>
</div>
</div>
</div>

<?php if(isset($ssy_email)) {

$totalInv = $ssy_deposit_amount_yearly * 14;

?> 


  <div class="ssy_result">
    <div class="container">
		
		
      <div class="main-result">
        <ul>
          <li> <strong>Interest<br>
            Rate</strong> <span><?php echo $interest_rate."0%"; ?></span> </li>
          <li> <strong>Total Investment of<br>
            Principal Amount</strong> <span><i><img src="<?php echo site_url('rupee-symbol.png'); ?>" alt=""> </i> <?php echo $totalInv; ?></span> </li>
          <li> <strong>Total Interest<br>
            Amount </strong> <span><i><img src="<?php echo site_url('rupee-symbol.png'); ?>" alt=""></i> <?php echo $totalnterest_amount; ?></span> </li>
          <li class="mature_amount"> <strong>Maturity <br>
            Amount</strong> <span><i><img src="<?php echo site_url('rupee-symbol.png'); ?>" alt=""></i> <?php echo $totamaturity_amount; ?></span> </li>
          <li> <strong>Maturity <br>
            Date</strong> <span><?php echo $maturedate; ?></span> </li>
        </ul>
        <div class="ssy_table">
          <table class="table">
            <thead>
              <tr>
                <th>Account Age</th>
                <th>Her Age</th>
                <th>Date of Deposit</th>
                <th>Yearly Deposit Amount</th>
                <th>Yearly Interest Amount </th>
                <th>Total Maturity Amount </th>
              </tr>
            </thead>
            <tbody>
            <?php
            	            	
            	$spl = explode("/",$ssy_dodeposit);
            	$amt = $ssy_deposit_amount_yearly;
            	$totalMat = 0;
            	$dD = $spl[2];
            	$class = "";
            	for($i = 1; $i <= 21; $i++)
            	{

                $y = $spl[2]++."-".$spl[1]."-".$spl[0];
                //$ssy_dob = date("Y-d-m",strtotime($ssy_dob));
                $date_diff = abs(strtotime($y) - strtotime($ssy_dob));
                $years = floor($date_diff / (365*60*60*24));
                
         				if($i == 15)
         				{
         					$amt = 0;
         				}

         				if($i > 18)
         				{
         					$class = "class='after-eighteen'";
         				}
         				$t = $amt+$totalMat;
         				$interestAmt = round(($t * $interest_rate)/100);
         				$totalMat = $amt + $interestAmt + $totalMat;

            		echo '<tr '.$class.'>
	                <td>'.$i.'</td>
	                <td>'.$years.'</td>
	                <td>'.$spl[0]."/".$spl[1]."/".$dD++.'</td>
	                <td>'.$amt.'</td>
	                <td>'.$interestAmt.'</td>
	                <td>'.$totalMat.'</td>
	              	</tr>';
            	}

            ?>

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

<?php } ?>

  <div class="content-panel">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="global-faqs">
            <h2 class="text-center"> FAQs on Sukanya Samriddhi Yojana Calculator </h2>
            <div class="wrapper">
              <h3><span class="question">Q</span> Who can use Sukanya Samriddhi Calculator? </h3>
              <div class="ans"> <p>All the persons who are planning to invest in Sukanya Samridhi Yojna(SSY), can use this calculator before taking the investment decision.This will help you in -</p>
				<ul>
				<li>Calculation of the returns on amount invested every year.</li>
<li>Total amount that you will receive at the time of maturity.</li>
<li>Date of Maturity(Approx).</li>				
				</ul>
				  <p>Please note that you can invest in SSY only when you meet the <a href="https://tax2win.in/guide/sukanya-samriddhi-yojana" target="_blank">eligibility criteria</a> i.e the age of girl child is not more than 10 years and the girl is a resident of India.</p>
				  </div>
				
              <hr class="hr">
            </div>
            <div class="wrapper">
              <h3><span class="question">Q</span> How does the Sukanya Samridhi Yojna Calculator work?</h3>
             <div class="ans">  <p>The calculator arrives at the maturity value and the total interest during the invested period, as per the SSY rules. However, while calculating the maturity value, it works on the following assumptions :</p>
              <ul>
               <li>Individuals contribute the same amount every year for 14 years.</li>
<li>There is no contribution made from year 15 to 21(as not mandated, though individuals are free to invest as per their wish). Interest is calculated on the basis of the previous contributions</li>
<li>Interest amount is calculated on the basis of the current interest rate for invested period.</li>

              </ul></div>
              <hr class="hr">
            </div>
            <div class="wrapper">
              <h3><span class="question">Q</span> What documents are required for opening Sukanya Samriddhi Account?</h3>
             <div class="ans">  <ul><li>Birth Certificate of the child</li>
<li>Address & Identity proof of the parents/guardian</li></ul></div>

              <hr class="hr">
            </div>
            <div class="wrapper">
              <h3><span class="question">Q</span> How many accounts can be opened under Sukanya Samriddhi Account Yojana (SSY)?</h3>
             <div class="ans">  <p>Under the SSY (Sukanya Samriddhi Yojana) you can open a maximum of two accounts at a time. But, in some special cases this number can exceed. These circumstances include</p>
				<ul>
				<li>Birth of twin sisters, when the first birth already gave life to a girl child.</li>
<li>Birth of triplets i.e. three children at one time all being girl children.</li>				
				</ul>
				<p>Even, in the above cases the opening of Sukanya Samriddhi Account is permitted only upon furnishing a valid medical certificate supporting the facts.The account can be applied by their parents or legal guardians.</p></div>
              <hr class="hr">
            </div>
            <div class="wrapper">
              <h3><span class="question">Q</span> Can a holder of PPF account also open Sukanya Samriddhi (SSY) Account?</h3>
             <div class="ans">  <p>Yes it is possible. Many people having <a class="link-p" href="https://tax2win.in/guide/public-provident-fund-ppf" target="_blank">PPF account</a> find Sukanya Samriddhi Account a great choice due to:</p>
				<ul>
				<li>Higher Interest Rates and Returns.</li>
<li>Overall increased investment limits.</li>
<li>Lower mandatory deposit requirements. In SSY you need to deposit a minimum of Rs 250 unlike the limit of Rs 500 for PPF.</li>

				</ul></div>
              <hr class="hr">
            </div>
            <div class="wrapper">
              <h3><span class="question">Q</span> How many SSY Accounts can be opened in one name?</h3>
            <div class="ans">   <p>Only one SSY account can be opened in one name. With, the maximum of two accounts for two girl children by parents or legal guardians.</p></div>
              <hr class="hr">
            </div>
            <div class="wrapper">
              <h3><span class="question">Q</span> From where can I open an SSY Account?</h3>
            <div class="ans">   <p>You can open SSY Account from any nearby post office or bank.</p></div>
              <hr class="hr">
            </div>
            <div class="wrapper">
              <h3><span class="question">Q</span> Is it possible to close SSY Account?</h3>
            <div class="ans">   <p>No, the SSY account cannot be permanently closed. You can stop making the contributions and take the deposited amount after completion of 21 years or when the girl child turns 18 for her marriage or higher education. </p></div>
              <hr class="hr">
            </div>
            <div class="wrapper">
              <h3><span class="question">Q</span> Is a loan facility available against SSY Account?</h3>
             <div class="ans">  <p>No, the loan facilities are not available against SSY Account.</p></div>
              <hr class="hr">
            </div>
            <div class="wrapper">
              <h3><span class="question">Q</span> How can tax savings maximised from SSY Account?</h3>
             <div class="ans">  <p>For Sukanya Samriddhi Yojana Account tax benefits are given on the basis of nomination. In the case of two girl children both mother and father can seek separate nominations and take benefit of Rs 1.5 lakh <a class="link-p" href="https://tax2win.in/guide/income-tax-deduction-section-80-c" target="_blank">under section 80C</a> each.</p></div>
              <hr class="hr">
            </div>
            <div class="wrapper">
              <h3><span class="question">Q</span> What will happen to SSY funds in case of the untimely demise of the account holder?</h3>
             <div class="ans">  <p>In such a situation the account will be closed immediately and funds would be handed over to the nominee.</p></div>
              <hr class="hr">
            </div>
            <div class="wrapper">
              <h3><span class="question">Q</span> Can I reactivate the Sukanya Samriddhi Yojana Account?</h3>
             <div class="ans">  <p>In case you skip to make the minimum contribution of Rs 250 in any of the year then your account is deactivated. To reactivate the account a fee of Rs 50 would be charged.</p></div>
              <hr class="hr">
            </div>
            <div class="wrapper">
              <h3><span class="question">Q</span> Can SSY Account premature?</h3>
           <div class="ans">    <p>Premature closure of SSY account is allowed only in these two cases:</p>
				<ul>
				<li>In case of account holder’s death.</li>
<li>In case funds are required for treating the life-threatening disease of the account holder.</li>
				</ul></div>
				
              <hr class="hr">
            </div>
            
          </div>
        </div>
      </div>
    </div>
    <br>
    <br class="hidden-xs">
    <br class="hidden-xs">
  </div>
</section>

