<?php
//form validation page

$config = array(
        array(
                'field' => 'ssy_email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email|xss_clean|callback_emailCheck'
        ),
        array(
                'field' => 'ssy_dob',
                'label' => 'DOB',
                'rules' => 'trim|required|exact_length[10]|xss_clean|regex_match[/^\d{2}\/\d{2}\/\d{4}$/]|callback_dateCheck'
        ),
        array(
                'field' => 'ssy_dodeposit',
                'label' => 'DOD',
                'rules' => 'trim|required|exact_length[10]|xss_clean|regex_match[/^\d{2}\/\d{2}\/\d{4}$/]|callback_dateToCheck'
        ),
        array(
                'field' => 'ssy_deposit_amount_yearly',
                'label' => 'Deposit Amount',
                'rules' => 'trim|required|numeric|greater_than[249]|less_than[150001]|xss_clean|callback_amountCheck'
        )
);

?>