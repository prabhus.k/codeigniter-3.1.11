<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_ssy_calculator extends CI_Migration {

        public function up()
        {
        	$this->dbforge->add_field(array(
                       
				'ssy_id' => array(
					'type' => 'INT',
					'constraint' => 11,
					'unsigned' => TRUE,
					'auto_increment' => TRUE
				),
				'ssy_email' => array(
					'type' => 'VARCHAR',
					'constraint' => '255',
					'null' => FALSE
				),
				'ssy_dob' => array(
					'type' => 'DATE',
					'null' => FALSE
				),
				'ssy_dodeposit' => array(
					'type' => 'VARCHAR',
					'constraint' => '32',
					'null' => FALSE
				),
				'ssy_deposit_amount_yearly' => array(
					'type' => 'INT',
					'constraint' => '10',
					'null' => FALSE
				),
				'totalnterest_amount' => array(
					'type' => 'INT',
					'constraint' => '10',
					'null' => FALSE
				),
				'totamaturity_amount' => array(
					'type' => 'INT',
					'constraint' => '10',
					'null' => FALSE
				),
				'interest_rate' => array(
					'type' => 'FLOAT',
					'null' => FALSE
				),
				'maturedate' => array(
					'type' => 'VARCHAR',
					'constraint' => '32',
					'null' => FALSE
				),
				'token' => array(
					'type' => 'TEXT',
					'constraint' => '32',
					'null' => FALSE
				),
				'created_date datetime default current_timestamp',				
										
			));
								
			$this->dbforge->add_key('ssy_id', TRUE);
			$this->dbforge->create_table('ssy_table', TRUE);

			//Create table for Interest Calculation

			$this->dbforge->add_field(array(
                       
				'interest_id' => array(
					'type' => 'INT',
					'constraint' => 11,
					'unsigned' => TRUE,
					'auto_increment' => TRUE
				),
				'period_from' => array(
					'type' => 'DATE',
					'null' => FALSE
				),
				'period_to' => array(
					'type' => 'DATE',
					'null' => FALSE
				),
				'interest_rate' => array(
					'type' => 'DOUBLE',
					'null' => FALSE
				),
				
				'created_date datetime default current_timestamp',				
										
			));
								
			$this->dbforge->add_key('interest_id', TRUE);
			$this->dbforge->create_table('check_interest', TRUE);

			/* Dumping Data 
			-------------------------------------------------
			INSERT INTO `check_interest` (`interest_id`, `period_from`, `period_to`, `interest_rate`) VALUES
					(1, '2014-12-03', '2015-03-31', 9.1),
					(2, '2015-04-01', '2016-03-31', 9.2),
					(3, '2016-04-01', '2016-09-30', 8.6),
					(4, '2016-10-01', '2017-03-31', 8.5),
					(5, '2017-04-01', '2017-06-30', 8.4),
					(6, '2017-07-01', '2017-12-31', 8.3),
					(7, '2018-01-01', '2018-09-30', 8.1),
					(8, '2018-10-01', '2019-06-30', 8.5),
					(9, '2019-07-01', '2020-03-31', 8.4),
					(10, '2020-04-01', '2021-09-30', 7.6);

			*/

		}

        public function down()
        {
                $this->dbforge->drop_table('ssy_table');
                
        }
}

?>
