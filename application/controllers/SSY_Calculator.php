<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');

class SSY_Calculator extends CI_Controller {

	public $intrAm =0, $totalMat1 =0;
    public $dateMature;

    function __construct()
	{
		parent::__construct();
		// Initializing the Model
		$this->load->model('Ssy_calculator_model');
	}
    
	public function emailCheck($ssy_email)
	{
		if (!filter_var(htmlspecialchars($ssy_email ,ENT_QUOTES), FILTER_SANITIZE_EMAIL, FILTER_VALIDATE_EMAIL)) { $this->form_validation->set_message('emailCheck', 'The {field} field must contain a valid email address.');
        	return FALSE;
    	}else{
    		return TRUE;
    	}
	}

	public function dateCheck($dates)
	{
		if (!filter_var(htmlspecialchars($dates ,ENT_QUOTES), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH)) { $this->form_validation->set_message('dateCheck', 'The {field} entered not correct format!');
        	return FALSE;
    	}else{
    		return TRUE;
    	}
	}	

	public function dateToCheck($dates)
	{
		if (!filter_var(htmlspecialchars($dates ,ENT_QUOTES), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH)) { 
			$this->form_validation->set_message('dateToCheck', 'The {field} entered not correct format!');
        	return FALSE;
    	}else{
    		$start_date=str_replace("/", "-", $this->input->post('ssy_dob'));
			$end_date=str_replace("/", "-",$this->input->post('ssy_dodeposit'));

			 if(strtotime($start_date) < strtotime($end_date))
             {
              	return TRUE;
             }
             else
             {
              	$this->form_validation->set_message('dateToCheck', 'DOD must be after Date of Birth');
             	return FALSE;
             }
    	}
	}	

	public function amountCheck($dates)
	{
		if (!filter_var(htmlspecialchars($dates,ENT_QUOTES), FILTER_VALIDATE_INT)) { 
			$this->form_validation->set_message('amountCheck', 'Please check the {field} field!');
        	return FALSE;
    	}else{
    		return TRUE;
    	}
	}	
	
	public function index()
	{
		//Helper - libary inclusion
		$this->load->helper('url');
		$this->load->helper('security');
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		
        if ($this->form_validation->run() == FALSE)
        {
        	$this->pageView("");        	
        }else 
		{
			$this->pageData();
		} 
	}

	public function pageData()
	{

		$data['ssy_email']=$this->security->xss_clean($this->input->post('ssy_email'));
		$dob_date = strtr($this->security->xss_clean($this->input->post('ssy_dob')), '/', '-');
		$data['ssy_dob']=date("Y-m-d",strtotime($dob_date));
		$data['ssy_dodeposit']=$this->security->xss_clean($this->input->post('ssy_dodeposit'));
		$data['ssy_deposit_amount_yearly']=$this->security->xss_clean($this->input->post('ssy_deposit_amount_yearly'));
		$result = $this->interestCalculation($this->input->post('ssy_dodeposit'), $data['ssy_deposit_amount_yearly']);
		$data = $data + $result;	
		$data['token'] = $this->security->get_csrf_hash(); //Getting Token
        $data = $this->security->xss_clean($data); //XSS Filtering
        $this->Ssy_calculator_model->insert($data);
		$this->pageView($data);
	}

	public function pageView($value)
	{
		
		$this->load->view('header');
		$this->load->view('ssy_calculator', $value);
		$this->load->view('footer');
		
	}

	public function interestCalculation($ssy_dodeposit, $ssy_deposit_amount_yearly)
	{
		
    	$amtTemp = $ssy_deposit_amount_yearly;
    	$matAm =0;
    	$interestRate = $this->getInterestRate($ssy_dodeposit); 
		for($i = 1; $i <= 21; $i++)
    	{    	
    		($i == 15) ? $amtTemp = 0 : '';	
			$tt = $amtTemp+$this->totalMat1;
			$interestAmtt = round(($tt * $interestRate)/100);
			$this->totalMat1 = $amtTemp + $interestAmtt + $this->totalMat1;
			$this->intrAm = $this->intrAm + $interestAmtt;
    		$matAm = $amtTemp + $interestAmtt + $matAm;
    	} 
    	
        $result['totalnterest_amount'] = $this->intrAm;
        $result['totamaturity_amount']  = $matAm;
        $result['maturedate']  = $this->dateMature;
        $result['interest_rate']  = $interestRate;
        return $result;

	}

	public function getInterestRate($ssy_dodeposit)
	{  		
   		//Interser Calculation Section
    	$split = explode("/",$ssy_dodeposit);
    	$cnt_date = $split[2]+21;
		$periodDate = $split[1]."/".$split[0]."/".$split[2];  
		$periodDate=date('Y-m-d', strtotime($periodDate));
		$this->dateMature = $split[0]."/".$split[1]."/".$cnt_date;
		$interestRate = $this->Ssy_calculator_model->fetch_interest($periodDate);
		return $interestRate;
	}
	
}

?>
