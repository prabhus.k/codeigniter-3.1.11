<?php

class SSY_Calculator_model extends CI_MODEL
{
	function insert($data) //This function is used to insert records into the table
	{
		$this->db->insert('ssy_table', $data);

	}

	function fetch_interest($dod) // fetch Interest Rate from Database
	{
		$id = $this->db->select('interest_rate')->where('"'.date('Y-m-d', strtotime($dod)).'" between period_from and period_to')->get('check_interest')->row();
		return $id->interest_rate;
		
	}
}

?>
