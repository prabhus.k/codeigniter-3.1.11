$(document).ready(function(){

     var date = new Date();
     date.setDate(date.getDate());
		
});

$(function(){

    $("#ssy_dob").datepicker({
        autoclose: true,
        format: 'dd/mm/yyyy'
    }).on('changeDate', function (selected) {
        var minDate = new Date(selected.date);
        minDate.setDate(minDate.getDate() + 1);
        $('#ssy_dodeposit').datepicker('setStartDate', minDate);
    });

    $("#ssy_dodeposit").datepicker({
        autoclose: true,
        format: 'dd/mm/yyyy'
    }).on('changeDate', function (selected) {
        var minDate = new Date(selected.date);
        minDate.setDate(minDate.getDate() - 1);
        $('#ssy_dob').datepicker('setEndDate', minDate);
    });
       
});